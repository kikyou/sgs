import Vue from '@vitejs/plugin-vue';
import { NaiveUiResolver } from 'unplugin-vue-components/resolvers';
import Components from 'unplugin-vue-components/vite';
import { defineConfig } from 'vite';
import tsAlias from 'vite-plugin-ts-alias';

export default defineConfig({
  publicDir: 'vitePublic',
  build: {
    cssCodeSplit: false,
    chunkSizeWarningLimit: 1600,
  },
  plugins: [
    Vue({
      reactivityTransform: true,
    }),
    tsAlias(),
    Components({
      resolvers: [NaiveUiResolver()],
    }),
  ],
  css: {
    preprocessorOptions: {
      scss: {
        additionalData: '@import "src/style/variable.scss"; @import "src/style/normalize.scss";',
      },
    },
  },
  preview: {
    open: true,
  },
});
