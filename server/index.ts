import loader from '@utils/loader';
import { forEach, map } from 'lodash/fp';
import { Server } from 'socket.io';
import type { TemplatedApp } from 'uWebSockets.js';
import { App } from 'uWebSockets.js';
import room from '@server/room';

console.clear();
// @ts-ignore
const app = new App() as TemplatedApp;
const io = new Server({
  cors: {
    origin: '*',
  },
});
io.attachApp(app);

// 为所有游戏模式创建namespace
const TESTROOM = '测试房间';
const { syncInput, setCurrentRoom } = room();
const modes = ['identity'];
forEach(async mode => {
  const modeNamespace = io.of(`/${mode}`);
  const m: useIdentity = await loader('mode', mode);

  modeNamespace.use((socket, next) => {
    const { clientName } = socket.handshake.auth;
    socket.data = socket.handshake.auth;
    console.log(`${clientName} 已连接`);
    next();
  });

  modeNamespace.on('connection', socket => {
    socket.join(TESTROOM);
    socket.on('disconnecting', reason => {
      console.log(`${socket.data.clientName} 即将断开连接，原因：${reason}`);
      syncInput({
        type: 'die',
        target: socket.id,
      });
    });
    socket.on('disconnect', () => {
      console.log(`${socket.data.clientName} 断开了连接`);
    });
  });
  modeNamespace.adapter.on('join-room', async (room, id) => {
    if (room !== id) {
      const socket = modeNamespace.sockets.get(id)!;
      const sockets = await modeNamespace.in(room).fetchSockets();
      const data = map(
        socket => ({
          socketId: socket.id,
          clientName: socket.data.clientName,
        }),
        sockets,
      );
      console.log(`${socket.data.clientName} 已加入房间 [${room}]`);
      socket.emit('query-room', data);
      modeNamespace.in(room).except(id).emit('join-room', socket.data.clientName);
      const { size } = modeNamespace.adapter.rooms.get(TESTROOM)!;
      if (size >= 2) {
        const Room = modeNamespace.in(room);
        setCurrentRoom(Room);
        syncInput({
          type: 'clear',
        });
        Room.emit('game-start');
        const ss = m(data);
        setTimeout(ss.start, 2000);
      }
    }
  });
  modeNamespace.adapter.on('leave-room', async (room, id) => {
    if (room !== id) {
      const socket = modeNamespace.sockets.get(id)!;
      console.log(`${socket.data.clientName} 已退出房间 [${room}]`);
      modeNamespace.in(room).emit('leave-room', socket.data.clientName);
    }
  });
}, modes);

app.listen(7777, token => {
  if (!token) {
    console.warn('端口被占用...');
  } else {
    console.log('成功启动服务器 端口7777...');
  }
});
