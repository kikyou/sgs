// Window 扩展
interface Window {
  [x: string]: any;
}

// 基本类型简写
type num = number;
type bool = boolean;
type str = string;

// 武将定义
type country = 'wei' | 'shu' | 'wu' | 'qun';
type sex = 0 | 1;
type hero = [str, country, sex, num, str[]];
type heros = hero[];
type listener = 'player' | 'global';

// 时机
type phase =
  | 'roundBegin'
  | 'phasePrepare'
  | 'phaseJudgeBegin'
  | 'phaseJudgeContent'
  | 'phaseJudgeEnd'
  | 'phaseDrawBegin'
  | 'phaseDrawContent'
  | 'phaseDrawEnd'
  | 'phaseUseBegin'
  | 'phaseUseContent'
  | 'phaseUseEnd'
  | 'phaseDiscardBegin'
  | 'phaseDiscardContent'
  | 'phaseDiscardEnd'
  | 'phaseEnd'
  | 'roundEnd'
  | 'damageWhen'
  | 'woundedWhen'
  | 'woundedContent'
  | 'damageAfter'
  | 'woundedAfter'
  | 'chooseToUse';

type atLeastOne<T, Keys extends keyof T = keyof T> = {
  [K in Keys]: Required<Pick<T, K>> & Partial<Exclude<T, K>>;
}[Keys];
type phases = atLeastOne<Record<listener, phase[]>>;
type filter = (e: ev) => bool;
type content = (e: ev) => Promise<void>;
interface commonSkill {
  /** **💡 执行条件** */
  filter?: filter;
  /** **💡 技能内容** */
  content: content;
}
interface triggerSkill extends commonSkill {
  type: 'triggerSkill';
  /** **💡 触发技时机** */
  trigger: phases;
}
interface enableSkill extends commonSkill {
  type: 'enableSkill';
  /** **💡 主动技时机** */
  enable: phase[];
}
type _skill = triggerSkill | enableSkill;
type skill = _skill & {
  /** **💡 拥有者** */
  owner: num;
  /** **💡 技能名** */
  name: str;
  /** **💡 全局** */
  global?: bool;
};
type _skills = Record<str, _skill>;
type skills = Record<str, skill>;

// 卡牌
/** **💡 花色** */
type suit = '♥' | '♠' | '♣' | '♦';
/** **💡 牌** */
type card = [str, num, suit];
/** **💡 牌(多张)** */
type cards = card[];

// 事件
type id = num;
type targets = num | num[];
interface e {
  /** **💡 事件来源(如果有)** */
  source?: num;
  /** **💡 事件目标** */
  target?: num;
  /** **💡 事件数量(如果有)** */
  num?: num;
  /** **💡 事件中涉及的牌(如果有)** */
  cards?: cards;
}
interface ev {
  /** **💡 事件id** */
  id: id;
  /** **💡 事件类型** */
  phase: phase;
  /** **💡 事件执行者** */
  player: num;
  /** **💡 原始事件对象** */
  e: e;
  /** **💡 事件来源(如果有)** */
  source?: num;
  /** **💡 事件目标** */
  target?: num;
  /** **💡 事件数量(如果有)** */
  num?: num;
  /** **💡 事件中涉及的牌(如果有)** */
  cards?: cards;
  /** **💡 事件结束函数** */
  finish: any;
}

type cardArea = 'h' | 'e' | 'j';
type cardAreaAll = 'h' | 'e' | 'j' | 'he' | 'hj' | 'ej' | 'hej';
type cardPosition = 'top' | 'bottom';
type executor = 'source' | 'target';
type exphase = `${executor}.${phase}`;
type progress = exphase[];
type handles = Record<str, skill[][]>;
/** **💡 翻译** */
type tr = Record<str, str>;
/** **💡 游戏所需的配置** */
type mode = 'identity' | 'guozhan' | 'xx';
type cardPileType = 'junzheng' | 'guozhan';

/** **💡 游戏** */
type playerNum = 2 | 3 | 5 | 8;
interface modeConfig {
  mode?: mode;
  playerNum: playerNum;
}

/** **💡 身份模式** */
type identity = 'zhu' | 'zhong' | 'nei' | 'fan' | 'unknown';
type identitys = identity[];
interface gamer {
  clientName: str;
}
type initDataList = {
  socketId: str;
  clientName: str;
}[];
