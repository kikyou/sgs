import { App as CapacitorApp } from '@capacitor/app';
import { createApp } from 'vue';
import { createPinia } from 'pinia';
import router from '@router/index';
import 'animate.css/animate.min.css';
import 'normalize.css';
import App from './App.vue';

if (/Android/i.test(navigator.userAgent)) {
  // @ts-ignore
  import('eruda').then(async ({ default: eruda }) => {
    eruda.init();
  });
  CapacitorApp.addListener('backButton', ({ canGoBack }) => {
    if (!canGoBack) {
      CapacitorApp.exitApp();
    } else {
      window.history.back();
    }
  });
}

const app = createApp(App);
app.use(router).use(createPinia()).mount('#root');
