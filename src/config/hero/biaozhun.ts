import sleep from '@utils/sleep';

const heros: heros = [
  ['liubei', 'shu', 1, 4, ['rende']],
  ['guanyu', 'shu', 1, 4, ['wusheng', 'yijue']],
  ['zhangfei', 'shu', 1, 4, ['paoxiao', 'tishen']],
  ['zhugeiang', 'shu', 1, 3, ['guanxing', 'kongcheng']],
  ['zhaoyun', 'shu', 1, 4, ['longdan', 'yajiao']],
  ['machao', 'shu', 1, 4, ['mashu', 'tieji']],
  ['huangyueying', 'shu', 1, 3, ['jizhi', 'qicai']],
  ['sunquan', 'wu', 1, 4, ['zhiheng', 'jiuyuan']],
  ['ganning', 'wu', 1, 4, ['qixi', 'fenwei']],
  ['lvmeng', 'wu', 1, 4, ['keji', 'qinxue']],
  ['huanggai', 'wu', 1, 4, ['kurou', 'zhaxiang']],
  ['zhouyu', 'wu', 1, 3, ['yingzi', 'fanjian']],
  ['daqiao', 'wu', 1, 3, ['guose', 'liuli']],
  ['luxun', 'wu', 1, 3, ['qianxun', 'lianying']],
  ['sunshangxiang', 'wu', 1, 3, ['jieyin', 'xiaoji']],
  ['caocao', 'wei', 1, 4, ['jianxiong', 'hujia']],
  ['simayi', 'wei', 1, 3, ['fankui', 'guicai']],
  ['xiahoudun', 'wei', 1, 4, ['ganglie', 'qingjian']],
  ['zhangliao', 'wei', 1, 4, ['tuxi']],
  ['xuchu', 'wei', 1, 4, ['luoyi']],
  ['guojia', 'wei', 1, 3, ['tiandu', 'yiji']],
  ['zhenji', 'wei', 1, 3, ['luoshen', 'qingguo']],
  ['huatuo', 'qun', 1, 3, ['jijiu', 'qingnang']],
  ['lvbu', 'qun', 1, 4, ['wushuang', 'liyu']],
  ['diaochan', 'qun', 1, 3, ['lijian', 'biyue']],
  ['huaxiong', 'qun', 1, 4, ['yaowu']],
  ['sunce', 'wu', 1, 4, ['hunzi']],
];
const herosSkills: _skills = {
  rende: {
    type: 'enableSkill',
    enable: ['phaseUseContent'],
    async content(e) {
      await sleep(1000);
    },
  },
  fankui: {
    type: 'triggerSkill',
    trigger: {
      player: ['woundedAfter'],
    },
    async content(e) {
      await sleep(1000);
    },
  },
  hunzi: {
    type: 'triggerSkill',
    trigger: { player: ['phasePrepare'] },
    async content(e) {
      // this.addSkills('yinghun', 'yingzi');
    },
  },
  yinghun: {
    type: 'triggerSkill',
    trigger: { player: ['phasePrepare'] },
    async content(e) {},
  },
  yingzi: {
    type: 'triggerSkill',
    trigger: {
      player: ['phaseDrawBegin'],
    },
    async content(e) {
      e.num!++;
    },
  },
  fanjian: {
    type: 'enableSkill',
    enable: ['phaseUseContent'],
    async content() {
      console.log('反间内容');
    },
  },
};
const allSkillsName = Object.keys(herosSkills);

export { heros, herosSkills, allSkillsName };
