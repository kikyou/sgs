// import { map } from 'lodash';
import { concat, assign, keys, map } from 'lodash/fp';
import * as biaozhun from './biaozhun';
import * as feng from './feng';

function typeOf(obj: Record<str, any>) {
  return Object.prototype.toString.call(obj).slice(8, -1).toLowerCase();
}

const merge = <T>(...rest: T[]): T => {
  const r0 = rest[0];
  const ks = keys(r0) as (keyof T)[];

  return Object.fromEntries(
    map(i => {
      if (typeOf(r0[i]) === 'array') {
        // @ts-ignore
        return [i, concat(...map(j => j[i], rest))];
      }
      // @ts-ignore
      return [i, assign({}, ...map(k => k[i], rest))];
    }, ks),
  ) as any as T;
};

const { heros, herosSkills, allSkillsName } = merge(biaozhun, feng);
const DEFAULT_HERO: hero = ['sunce', 'wu', 1, 4, ['hunzi']];
export { heros, herosSkills, allSkillsName, DEFAULT_HERO };
