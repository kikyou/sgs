const heros: heros = [
  ['xiaoqiao', 'wu', 0, 3, ['tianxiang', 'hongyan']],
  ['xiahouyuan', 'wei', 1, 4, ['shensu']],
];
const herosSkills: _skills = {
  tianxiang: {
    type: 'triggerSkill',
    trigger: {
      player: ['woundedAfter'],
    },
    async content(e) {},
  },
  hongyan: {
    type: 'triggerSkill',
    trigger: {
      player: ['woundedAfter'],
    },
    async content(e) {},
  },
};
const allSkillsName = Object.keys(herosSkills);

export { heros, herosSkills, allSkillsName };
