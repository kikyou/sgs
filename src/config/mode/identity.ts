import useRoom from '@server/room';
import loader from '@utils/loader';
import { compose, concat, drop, prop, shuffle } from 'lodash/fp';
import { map } from '@utils/fp';

const room = useRoom();
const { syncInput } = room;

export default function useIdentity(initDataList: initDataList) {
  const setPlayers = () => {
    syncInput({
      type: 'setPlayers',
      data: initDataList.length,
    });
  };
  /** **💡 打乱牌堆** */
  const setCardPile = async () => {
    const cardPile: cards = await loader('card', 'junzheng');
    syncInput({
      type: 'setCardPile',
      data: cardPile,
    });
  };
  /** **💡 设置身份与座位** */
  const setIdentitysAndSeats = () => {
    const identityDict = {
      2: ['zhu', 'fan'],
      3: ['zhu', 'nei', 'fan'],
      5: ['zhu', 'zhong', 'nei', 'fan', 'fan'],
      8: ['zhu', 'zhong', 'zhong', 'nei', 'fan', 'fan', 'fan', 'fan'],
    };
    const fn = (identity: identity, index: num) => ({
      seat: index,
      identity,
      ...initDataList[index],
    });
    syncInput({
      type: 'setPlayers',
      data: compose(
        map(fn),
        concat('zhu'),
        shuffle,
        drop(1),
        prop(String(initDataList.length)),
      )(identityDict),
    });
  };

  const start = async () => {
    await setCardPile();
    setPlayers();
    setIdentitysAndSeats();
  };
  return {
    start,
  };
}
declare global {
  type useIdentity = typeof useIdentity;
  type useIdentityReturn = ReturnType<typeof useIdentity>;
}
