import biaozhun from '@config/hero/biaozhun_tr.json';
import feng from '@config/hero/feng_tr.json';

type getTr = (key: str) => str;
type getTrs = (keys: str[]) => str;
const trGame: tr = {
  zhu: '主',
  zhong: '忠',
  nei: '内',
  fan: '反',
  wuming: '无名',
};
const trMarks: tr = {
  ren: '忍',
  quanji: '权计',
  tian: '田',
};
const trAll: Record<str, str> = {
  ...biaozhun,
  ...feng,
  ...trGame,
  ...trMarks,
};

const tr: getTr = key => trAll[key] || key;
const trs: getTrs = keys => keys.reduce((acc, cur) => `${acc}${tr(cur)} `, '');
export { tr, trs };
