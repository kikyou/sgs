import { createRouter, createWebHashHistory } from 'vue-router';

const Home = () => import('../views/home.vue');
const Game = () => import('../views/game.vue');
const Room = () => import('../views/room.vue');

const routes = [
  { path: '/', redirect: '/home/电脑' },
  { path: '/home/:clientName', name: 'home', component: Home, props: true },
  { path: '/game', name: 'game', component: Game, props: true },
  { path: '/room', name: 'room', component: Room, props: true },
];

const router = createRouter({
  history: createWebHashHistory(),
  routes,
});
export default router;
