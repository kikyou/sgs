import type { Socket } from 'socket.io-client';

const to = (
  socket: Socket,
  name: string,
  data: Record<string, any>,
  cb: (back: any[]) => void,
): Promise<void> =>
  new Promise(res => {
    socket.emit(name, data, (back: any) => {
      console.log(back);
      cb(back);
      res(back);
    });
  });
export default to;
