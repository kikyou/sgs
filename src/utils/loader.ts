type loader = (type: str, name: str) => Promise<any>;

const loader: loader = (type, name) =>
  new Promise((res, rej) => {
    import(`../config/${type}/${name}.ts`)
      .then(({ default: m }) => {
        res(m);
      })
      .catch(e => {
        console.log(e);
        console.log(`${type}-${name} 加载失败...`);
        rej();
      });
  });
export default loader;
