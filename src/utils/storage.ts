interface storage {
  set(key: str, value: any): void;
  get(key: str): any;
  rm(key: str): void;
  clear(): void;
}

const storage: storage = {
  set(key, value) {
    localStorage.setItem(key, JSON.stringify(value));
  },
  get(key) {
    const value = localStorage.getItem(key);
    try {
      return JSON.parse(value!);
    } catch (e) {
      return value;
    }
  },
  rm(key) {
    localStorage.removeItem(key);
  },
  clear() {
    localStorage.clear();
  },
};
export default storage;
