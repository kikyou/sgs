import {
  map as oldMap,
  forEach as oldForEach,
  type LodashMap,
  type LodashForEach,
} from 'lodash/fp';
// @ts-ignore
export const map = oldMap.convert({ cap: false }) as LodashMap;
// @ts-ignore
export const forEach = oldForEach.convert({ cap: false }) as LodashForEach;
