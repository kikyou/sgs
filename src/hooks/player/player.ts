import { ref, computed } from 'vue';
import { DEFAULT_HERO, heros, herosSkills, allSkillsName } from '@config/hero/index';
import { find, forEach, includes, filter, pullAll, prop, clone } from 'lodash/fp';
import { tr, trs } from '@config/tr';

const hero = DEFAULT_HERO;

const usePlayer = () => {
  const socketId = ref('');
  const clientName = ref('');
  /** **💡 姓名** */
  const name = ref(hero[0]);
  /** **💡 国籍** */
  const country = ref(hero[1]);
  /** **💡 性别** */
  const sex = ref(hero[2]);
  /** **💡 初始体力上限** */
  const initMaxHp = ref(hero[3]);
  /** **💡 初始技能列表** */
  const initSkillsConfig = ref(hero[4]);
  /** **💡 技能列表** */
  const skillsConfig = ref(hero[4]);
  /** **💡 体力** */
  const hp = ref(hero[3]);
  /** **💡 体力上限** */
  const maxHp = ref(0);
  /** **💡 座次** */
  const seat = ref(-1);
  /** **💡 身份** */
  const identity = ref('unknown');
  const isAlive = ref(true);
  const isTurnOut = ref(false);
  const isLinked = ref(false);
  const marks = ref<Record<str, number>>({});
  const isCurrent = ref(false);
  const storage = ref<Record<str, any>>({});
  const h = ref([]);
  const e = ref([]);
  const j = ref([]);

  const init = (heroName: str) => {
    const hero = find((h) => h[0] === heroName, heros) || DEFAULT_HERO;
    name.value = hero[0];
    country.value = hero[1];
    sex.value = hero[2];
    initMaxHp.value = hero[3];
    resetHp();
    initSkillsConfig.value = hero[4];
    skillsConfig.value = hero[4];
  };

  const turnOut = (bool = true) => {
    isTurnOut.value = !!bool;
    console.log(`${tr(name.value)}已${isTurnOut.value ? '翻面' : '翻回正面'}`);
  };

  const link = (bool = true) => {
    isLinked.value = !!bool;
    console.log(`${tr(name.value)}已${isLinked.value ? '横置' : '重置'}`);
  };

  const reset = () => {
    turnOut(false);
    link(false);
  };

  const addMarks = (mark: str, num = 1) => {
    if (mark in marks.value) {
      marks.value[mark] += num;
    } else {
      marks.value[mark] = num;
    }
    console.log(`${tr(name.value)}获得了${num}个【${tr(mark)}】标记`);
  };

  const rmMarks = (mark: str, num = 1) => {
    if (mark in marks.value) {
      marks.value[mark] -= num;
      console.log(`${tr(name.value)}失去了${num}个【${tr(mark)}】标记`);
    }
  };

  const getStorage = (key: str) => {
    return storage.value[key];
  };

  const setStorage = (key: str, value: any) => {
    storage.value[key] = value;
  };

  const rmStorage = (key: str) => {
    delete storage.value[key];
  };

  const die = () => {
    isAlive.value = false;
    reset();
    h.value = [];
    e.value = [];
    j.value = [];
    marks.value = {};
    storage.value = {};
    console.log(`${tr(name.value)}阵亡`);
  };

  const gainMaxHp = (num = 1) => {
    maxHp.value += num;
  };

  const linkSkill = (skillName: str) => {
    let skill = prop(skillName, herosSkills) as skill;
    if (skill) {
      skill = clone(skill);
      skill.name = skillName;
      skill.owner = seat.value;
      skill.global = false;
    } else {
      console.log('技能不存在');
    }
    return skill;
  };

  const linkSkills = (skillsConfig: str[]) => {
    const skills: skills = {};
    forEach((skillName) => {
      skills[skillName] = linkSkill(skillName);
    }, skillsConfig);
    return skills;
  };

  const skills = computed(() => linkSkills(skillsConfig.value));

  const loseMaxHp = (num = 1) => {
    maxHp.value -= num;
    console.log(`${tr(name.value)}失去了${num}点体力上限`);
    if (maxHp.value <= 0) {
      die();
    } else if (hp.value > maxHp.value) {
      hp.value = maxHp.value;
    }
  };

  const damage = async (targets: targets, num = 1, cards = []) => {
    const t: targets = ([] as num[]).concat(targets);
    if (num > 0) {
      forEach(async (target) => {
        const e = {
          source: seat.value,
          target,
          num,
          cards
        };
        window.e.create('damage', e);
      }, t);
    }
  };

  const woundedContent = (ev: ev) => {
    if (ev.num! <= 0) {
      ev.finish();
    } else {
      hp.value -= ev.num!;
      console.log(`${tr(name.value)}受到了${ev.num}点伤害`);
      if (hp.value <= 0) {
        die();
      }
    }
  };

  const loseHp = (num = 1) => {
    hp.value -= num;
    console.log(`${tr(name.value)}失去了${num}点体力`);
  };

  const recover = (num = 1) => {
    if (hp.value + num <= maxHp.value) {
      hp.value += num;
      console.log(`${tr(name.value)}回复了${num}点体力`);
    }
  };

  const recoverFullHp = () => {
    hp.value = maxHp.value;
    console.log(`${tr(name.value)}回复了所有体力`);
  };

  const resetHp = () => {
    maxHp.value = initMaxHp.value;
    hp.value = initMaxHp.value;
  };

  const hasSkills = (name: str) => {
    return includes(name, skillsConfig.value);
  };

  const addSkills = (...skc: str[]) => {
    const arr = filter((i) => {
      const bool = includes(i, allSkillsName);
      const bool2 = !includes(i, skillsConfig.value);
      return bool && bool2;
    }, skc);
    if (arr.length) {
      skillsConfig.value.push(...arr);
      console.log(`${tr(name.value)} 获得了${trs(arr)}`);
    }
  };

  const rmSkills = (...skc: str[]) => {
    const arr = filter((i) => {
      const bool = includes(i, allSkillsName);
      const bool2 = !includes(i, skillsConfig.value);
      return bool && !bool2;
    }, skc);
    if (arr.length) {
      pullAll(arr, skillsConfig.value);
      console.log(`${tr(name.value)} 失去了${trs(arr)}`);
    }
  };

  const clearSkills = () => {
    skillsConfig.value = [];
    console.log(`${tr(name.value)} 失去了所有技能`);
  };

  return {
    socketId,
    clientName,
    name,
    country,
    sex,
    initMaxHp,
    initSkillsConfig,
    skillsConfig,
    hp,
    maxHp,
    seat,
    identity,
    isAlive,
    isTurnOut,
    isLinked,
    marks,
    isCurrent,
    storage,
    h,
    e,
    j,
    skills,

    // Methods
    init,
    turnOut,
    link,
    reset,
    addMarks,
    rmMarks,
    getStorage,
    setStorage,
    rmStorage,
    die,
    gainMaxHp,
    loseMaxHp,
    damage,
    woundedContent,
    loseHp,
    recover,
    recoverFullHp,
    resetHp,
    hasSkills,
    addSkills,
    rmSkills,
    clearSkills,
    linkSkill,
    linkSkills
  };
};

export default usePlayer;

declare global {
  type player = ReturnType<typeof usePlayer>;
  type players = player[];
}
