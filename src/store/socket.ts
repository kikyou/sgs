import { io, type Socket } from 'socket.io-client';
import { defineStore } from 'pinia';
import game from '@store/game';
import { useRouter, useRoute } from 'vue-router';

const URL = import.meta.env.VITE_SOCKET_URL;
const { applyInput } = game();

// 使用setup模式定义
const useSocket = defineStore('useSocket', () => {
  let socket = $ref({}) as any as Socket;
  const clientName = sessionStorage.getItem('clientName');
  const router = useRouter();
  const route = useRoute();
  const connect = (namespace: mode) => {
    if (socket.connected === false) {
      socket.connect();
    } else if (socket.connected === undefined) {
      socket = io(`${URL}/${namespace}`, {
        auth: {
          clientName,
        },
      });
      socket.on('connect', () => {
        console.log(`已进入 ${namespace} 模式`);
      });
      socket.on('disconnect', () => {
        console.log('已断开连接');
        console.log(route);
        if (route.name === 'game') {
          router.back();
          applyInput({ type: 'clear' });
        }
      });
      socket.on('connect_error', err => {
        console.log(err.message); // not authorized
        // console.log(err.data); // { content: "Please retry later" }
      });
      socket.on('applyInput', (input: gameInput) => {
        applyInput(input);
      });
    }
  };
  return { clientName, socket: $$(socket), connect };
});
export default useSocket;
