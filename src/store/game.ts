import { defineStore } from 'pinia';
import usePlayer from '@/hooks/player/player';
import { map } from 'ramda';
import { forEach } from 'lodash';

const game = defineStore('game', {
  state: () => ({
    players: [] as players,
    cardPile: [] as cards,
  }),
  actions: {
    applyInput(input: gameInput) {
      console.log(input);
      switch (input.type) {
        case 'setPlayers':
          this.players = map(() => usePlayer(), new Array(input.data));
          break;
        case 'setCardPile':
          this.cardPile = input.data;
          break;
        case 'setAttr':
          forEach(this.players, (player, index) => {
            player.setAttr(input.data[index]);
          });
          break;
        case 'clear':
          this.players = [];
          this.cardPile = [];
          break;
        default:
          break;
      }
    },
  },
});
declare global {
  type gameInput = setPlayers | setCardPile | setAttr | clear;
}
type setPlayers = {
  type: 'setPlayers';
  data: num;
};
type setCardPile = {
  type: 'setCardPile';
  data: cards;
};
type setAttr = {
  type: 'setAttr';
  data: players;
};
type clear = {
  type: 'clear';
};
export default game;
